class MoviesController < ApplicationController

  def show
    id = params[:id] # retrieve movie ID from URI route
    @movie = Movie.find(id) # look up movie by unique ID
    # will render app/views/movies/show.<extension> by default
  end

  def init_checkbox (ratings)
    result = {}
    ratings.each do |rating|
      result[rating] = true
    end
    result
  end

  def session_for_movies (params)
    session[:id] = params[:id] if !params[:id].nil?
    session[:ratings] = params[:ratings] if !params[:ratings].nil?
  end

  def index
    @all_ratings = Movie.all_ratings
    @movies, @title_css, @date_css =  Movie.where(rating: @all_ratings), "", ""
    @checked = init_checkbox @all_ratings
    session_for_movies params
    if session[:id] == "title_header"
      @movies, @title_css, @date_css = Movie.order(:title), "hilite", ""
    elsif session[:id] == "release_date_header"
      @movies, @title_css, @date_css = Movie.order(:release_date), "", "hilite"
    end
    if !session[:ratings].nil?
      rating_select = session[:ratings].keys
      @movies = @movies.where(rating: rating_select)
      @all_ratings.each do |rating|
        if rating_select.index(rating).nil?
          @checked[rating] = false
        else
          @checked[rating] = true
        end
      end
    end
    keep_info_in_uri params
  end

  def session_match_params? params
    if session[:id] ==  params[:id] && session[:ratings] == params[:ratings]
      return true
    else
      return false
    end
  end

  def keep_info_in_uri params
    reply_params = Hash.new
    if !session[:ratings].nil?
      session[:ratings].keys.each do |rating|
        reply_params["ratings[#{rating}]"] = session[:ratings][rating]
      end
    end
    reply_params["id"] = session[:id] if !session[:id].nil?
    if !session_match_params? params
      flash.keep
      redirect_to movies_path(reply_params)
    end
  end

  def new
    # default: render 'new' template
  end

  def create
    @movie = Movie.create!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully created."
    redirect_to movies_path
  end

  def edit
    @movie = Movie.find params[:id]
  end

  def update
    @movie = Movie.find params[:id]
    @movie.update_attributes!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully updated."
    redirect_to movie_path(@movie)
  end

  def destroy
    @movie = Movie.find(params[:id])
    @movie.destroy
    flash[:notice] = "Movie '#{@movie.title}' deleted."
    redirect_to movies_path
  end

end
